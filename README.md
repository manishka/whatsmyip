# README #


### What is this repository for? ###

Whatsmyip project let's you know your Public IPv4 address.

* It uses [IPIFY API](https://www.ipify.org/) to request the IPv4 address
* It is implemented in two languages Python and HTML
* Version 0.1

### How do I get set up? ###

* Choose & download the file you want
* For HTML file: Open it in any web browser (Chrome, IE, Firefox, etc.)
* For Python file: Run it using python interpreter
* Python code depends on [requests ](https://pypi.python.org/pypi/requests) library

### Who do I talk to? ###

* Repo owner [@manishkukreja](https://twitter.com/manishkukreja)