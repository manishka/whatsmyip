# This example requires the requests library be installed. 
import requests
import time, os
while (True):
	try:
		while (True):
			ip = requests.get('https://api.ipify.org').text
			print time.strftime("%c"), 'My public IP address is:', ip
	except KeyboardInterrupt:
		print ""
		break
	except requests.ConnectionError as e:
		print time.strftime("%c"), 'Connect Failed'
		continue